package bsa.java.concurrency.fs;

import bsa.java.concurrency.exceptions.ApplicationException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;

@Component
@Slf4j
public class DiskFileSystem implements FileSystem {
	
	private static final String FILES_ROOT = "files";
	
	@Override
	@Async
	public CompletableFuture<String> saveFile(String fileName, byte[] bytes) {
		String filePath = FILES_ROOT + "/" + fileName;
		log.debug("Saving file {}", fileName);
		File savedFile = new File(filePath);
		//noinspection ResultOfMethodCallIgnored
		savedFile.getParentFile().mkdirs();
		try (FileOutputStream fos = new FileOutputStream(savedFile)) {
			fos.write(bytes);
			log.debug("Saved file '{}'", fileName);
			return CompletableFuture.completedFuture(filePath);
		} catch (IOException e) {
			log.error("File '{}' not saved, got an error: {}", fileName, e.getLocalizedMessage());
			throw new ApplicationException(e);
		}
	}
	
	@Override
	public void deleteAll() {
		log.debug("Deleting all files...");
		File file = new File(FILES_ROOT);
		if (file.exists()) {
			try {
				FileUtils.deleteDirectory(file);
				log.debug("Deleted all files");
			} catch (IOException e) {
				log.error("Files not deleted, got an error: {}", e.getLocalizedMessage());
				throw new ApplicationException(e);
			}
		}
		log.debug("Root folder '{}' did not exist. Already deleted.", FILES_ROOT);
	}
	
	@Override
	public void deleteFile(String filePath) {
		log.debug("Deleting file '{}'", filePath);
		File file = new File(filePath);
		if (file.exists()) {
			if (!file.delete()) {
				log.warn("File '{}' can't be deleted! ", filePath);
			} else {
				log.debug("File '{}' deleted", filePath);
			}
		} else {
			log.debug("File '{}' does not exist", filePath);
		}
	}
}
