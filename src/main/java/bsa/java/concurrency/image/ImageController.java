package bsa.java.concurrency.image;

import bsa.java.concurrency.exceptions.ApplicationException;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/image")
public class ImageController {
	private ImageService imageService;
	
	@Autowired
	public ImageController(ImageService imageService) {
		this.imageService = imageService;
	}
	
	@PostMapping("/batch")
	@ResponseStatus(HttpStatus.CREATED)
	public void batchUploadImages(@RequestParam("images") MultipartFile[] files) {
		try {
			for (MultipartFile file : files) {
				imageService.uploadImage(file.getBytes(), file.getOriginalFilename());
			}
		} catch (IOException e) {
			throw new ApplicationException(e);
		}
	}
	
	@PostMapping("/search")
	@ResponseStatus(HttpStatus.OK)
	public List<SearchResultDTO> searchMatches(@RequestParam("image") MultipartFile file,
											   @RequestParam(value = "threshold", defaultValue = "0.9") double threshold) {
		try {
			return imageService.searchMatches(file.getBytes(), threshold);
		} catch (IOException e) {
			throw new ApplicationException(e);
		}
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteImage(@PathVariable("id") UUID imageId) {
		imageService.deleteById(imageId);
	}
	
	@DeleteMapping("/purge")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void purgeImages() {
		imageService.deleteAll();
	}
}
