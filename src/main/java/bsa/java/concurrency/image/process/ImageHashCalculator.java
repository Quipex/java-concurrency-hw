package bsa.java.concurrency.image.process;

import bsa.java.concurrency.exceptions.ApplicationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;

@Slf4j
@Component
public class ImageHashCalculator {
	
	private static final int SHRINK_WIDTH = 9;
	private static final int SHRINK_HEIGHT = 9;
	
	@Async
	public CompletableFuture<Long> dHash(byte[] bytes) {
		try {
			log.debug("Calculating hash...");
			long hash = 0L;
			BufferedImage coloredImage = ImageIO.read(new ByteArrayInputStream(bytes));
			BufferedImage resizedGrayScaled = resizeAndGrayscale(coloredImage, SHRINK_WIDTH, SHRINK_HEIGHT);
			
			int[][] colors = getColors(resizedGrayScaled);
			for (int i = 1; i < SHRINK_WIDTH; i++) {
				for (int j = 1; j < SHRINK_HEIGHT; j++) {
					if (colors[i][j] > colors[i - 1][j - 1]) {
						hash |= 1;
					}
					hash = hash << 1;
				}
			}
			log.debug("Hash calculated");
			return CompletableFuture.completedFuture(hash);
		} catch (IOException e) {
			throw new ApplicationException(e);
		}
	}
	
	/**
	 * @param image gray scaled image
	 * @return 255 - white, 0 - black
	 */
	private static int[][] getColors(BufferedImage image) {
		int width = image.getWidth();
		int height = image.getHeight();
		int[][] colors = new int[width][height];
		int[] pixels = image.getData().getPixels(0, 0, width, height, new int[width * height]);
		int pixelIndex = 0;
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				colors[i][j] = pixels[pixelIndex++];
			}
		}
		return colors;
	}
	
	private static BufferedImage resizeAndGrayscale(BufferedImage image, int newWidth, int newHeight) {
		Image scaled = image.getScaledInstance(newWidth, newHeight, Image.SCALE_FAST);
		BufferedImage output = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_BYTE_GRAY);
		var g = output.createGraphics();
		g.drawImage(scaled, 0, 0, null);
		g.dispose();
		return output;
	}
}
