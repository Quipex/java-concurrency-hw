package bsa.java.concurrency.image.dto;

public interface SearchResultDTO {
    String getImageId();
    Double getMatchPercent();
    String getImageUrl();
}
