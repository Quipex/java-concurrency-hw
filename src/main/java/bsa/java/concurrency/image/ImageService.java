package bsa.java.concurrency.image;

import bsa.java.concurrency.exceptions.ApplicationException;
import bsa.java.concurrency.exceptions.FileNotFoundException;
import bsa.java.concurrency.fs.FileSystem;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.process.ImageHashCalculator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service
@Slf4j
public class ImageService {
	private FileSystem fileSystem;
	private ImageRepository imageRepository;
	private ImageHashCalculator hashCalculator;
	
	@Autowired
	public ImageService(FileSystem fileSystem, ImageRepository imageRepository, ImageHashCalculator hashCalculator) {
		this.fileSystem = fileSystem;
		this.imageRepository = imageRepository;
		this.hashCalculator = hashCalculator;
	}
	
	public List<SearchResultDTO> searchMatches(byte[] imageBytes, double threshold) {
		try {
			log.debug("Searching similar images...");
			Long hash = hashCalculator.dHash(imageBytes).get();
			List<SearchResultDTO> searchResult = imageRepository.searchSimilarImages(hash, threshold);
			log.debug("Done searching similar images");
			return searchResult;
		} catch (ExecutionException e) {
			throw new ApplicationException(e);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new ApplicationException(e);
		}
	}
	
	public void deleteById(UUID imageId) {
		log.debug("Deleting image with id {}", imageId);
		Image image = imageRepository.findById(imageId).orElseThrow(() -> new FileNotFoundException("No image with id " + imageId));
		fileSystem.deleteFile(image.getPath());
		imageRepository.deleteById(imageId);
		log.debug("Deleted image with id {}", imageId);
	}
	
	public void deleteAll() {
		log.debug("Purging all images...");
		fileSystem.deleteAll();
		imageRepository.deleteAllInBatch();
		log.debug("Purged all images");
	}
	
	@Async
	public void uploadImage(byte[] imageBytes, String originalFilename) {
		try {
			log.debug("Uploading new file...");
			UUID newFileId = UUID.randomUUID();
			log.debug("Calculating hash and saving to fs...");
			CompletableFuture<Long> hashCalc = hashCalculator.dHash(imageBytes);
			CompletableFuture<String> fileSave = fileSystem.saveFile(
					composeFileName(newFileId.toString(), fileExtension(originalFilename)), imageBytes);
			
			CompletableFuture.allOf(hashCalc, fileSave).join();
			log.debug("Done calculating and saving to fs");
			log.debug("Saving to db...");
			Long calculatedHash = hashCalc.get();
			String savedPath = fileSave.get();
			imageRepository.save(new Image(newFileId, calculatedHash, savedPath));
			log.debug("Done saving to db");
			log.debug("Done uploading new file");
		} catch (ExecutionException e) {
			throw new ApplicationException(e);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new ApplicationException(e);
		}
	}
	
	private String fileExtension(String originalFilename) {
		return originalFilename.substring(originalFilename.lastIndexOf(".") + 1);
	}
	
	private String composeFileName(String filename, String extension) {
		return filename + "." + extension;
	}
}
