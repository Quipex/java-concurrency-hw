package bsa.java.concurrency.image;

import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ImageRepository extends JpaRepository<Image, UUID> {
	String contextPath = "http://localhost:6968/";
	
	@Query(value =
			"SELECT cast(i.id as text) as imageId, " +
					"similarity(i.hash, :hash) * 100 as matchPercent, " +
					"'" + contextPath + "' || i.path as imageUrl " +
			"FROM images i WHERE similarity(i.hash, :hash) >= :threshold",
			nativeQuery = true)
	List<SearchResultDTO> searchSimilarImages(Long hash, double threshold);
}
