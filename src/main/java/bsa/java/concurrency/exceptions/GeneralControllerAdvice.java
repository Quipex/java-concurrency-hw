package bsa.java.concurrency.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GeneralControllerAdvice {
	
	@ExceptionHandler(FileNotFoundException.class)
	public ResponseEntity<String> handleNotFound(FileNotFoundException ex) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getLocalizedMessage());
	}
}
