package bsa.java.concurrency.exceptions;

public class FileNotFoundException extends RuntimeException {
	public FileNotFoundException(String message) {
		super(message);
	}
}
