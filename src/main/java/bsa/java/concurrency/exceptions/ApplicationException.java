package bsa.java.concurrency.exceptions;

public class ApplicationException extends RuntimeException {
	public ApplicationException(Throwable cause) {
		super(cause);
	}
}
