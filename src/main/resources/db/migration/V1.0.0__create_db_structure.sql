CREATE TABLE images(
    id UUID not null,
    hash bigint,
    path VARCHAR(255),
    PRIMARY KEY (id)
);
