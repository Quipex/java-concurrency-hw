CREATE OR REPLACE FUNCTION similarity(hash1 BIGINT, hash2 BIGINT) RETURNS FLOAT AS
$$
BEGIN
    RETURN 1 - length(replace((hash1 # hash2)::BIT(64)::TEXT, '0', ''))::FLOAT / 64;
END;
$$ LANGUAGE PLPGSQL;
